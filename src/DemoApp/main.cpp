#include "compute/Compute.h"

#include <iostream>

using namespace units::literals;

int main() {
    const auto width  = 2_m;
    const auto height = 3_m;
    std::cout << "Area of a rectangle of size [" << width << " x " << height << "]: ";
    std::cout << compute::rectangleArea(width, height);
    std::cout << "\n";
}
