#include "compute/Compute.h"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Testing compute::rectangleArea()") {
    using namespace units::literals;
    REQUIRE(compute::rectangleArea(1_m, 1_m) == 1_sq_m);
    REQUIRE(compute::rectangleArea(1_m, 2_m) == 2_sq_m);
    REQUIRE(compute::rectangleArea(2_m, 2_m) == 4_sq_m);
}
