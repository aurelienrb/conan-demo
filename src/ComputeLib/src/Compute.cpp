#include "compute/Compute.h"

namespace compute {

    units::area::square_meter_t rectangleArea(
        units::length::meter_t width, units::length::meter_t height) {
        return width * height;
    }

} // namespace compute