# Conan Demo

Basic C++ project using Conan.

## Install Conan

```
sudo apt install -y python3-pip
pip install conan
conan --version
```

## Build the project and run the unit tests

```
mkdir build && cd build
cmake ..
cmake --build .
ctest -V
```
